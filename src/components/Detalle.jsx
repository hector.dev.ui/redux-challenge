import React from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {unPokeDetalleAccion} from '../redux/pokeDucks'

export const Detalle = () => {
    const dispatch = useDispatch()
    React.useEffect(() => {
        const fetchData = () => {
            dispatch(unPokeDetalleAccion())
        }
        fetchData()
    }, [dispatch])

    const pokemon = useSelector(store => store.pokemones.unPokemon)
    console.log(pokemon);

    return pokemon ?(
        <div className="card mt-5">
            <div className="card-body">
            <img src={pokemon.foto} className="img-fluid" alt="pokemon-img"/>
                <div className="card-title">
                    {pokemon.nombre}
                    <p className="card-text">{pokemon.alto} || {pokemon.ancho}</p>
                </div>
            </div>
        </div>
    ): null
}
