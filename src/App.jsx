import React from "react";
import Pokemones from "./components/Pokemones";
import { Provider } from "react-redux";
import generateStore from "./redux/store";
import { Homepage } from "./pages/Homepage";
function App() {
  const store = generateStore();

  return (
    <Provider store={store}>
      <Homepage/>
      <div className="container mt-3">
        <Pokemones />
      </div>
    </Provider>
  );
}

export default App;
